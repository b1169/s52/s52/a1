function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.

    if(letter.length == 1)

    {

    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.

        for(let i=0; i<sentence.length; i++)
        {
            if(letter == sentence[i])
            {
                result++
            }
        }

        return result
    }
   
    // If letter is invalid, return undefined.

    else
    {
        return undefined
    }
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.

    let isogramLowercase = text.toLowerCase();
    let counter = 0;

    for(let i=0; i<isogramLowercase.length; i++)
    {

        for(let j=0; j<isogramLowercase.length; j++)
        {
            if(i != j && isogramLowercase[i] == isogramLowercase[j])
            {
                counter++;
            }
        }
    }

    // If the function finds a repeating letter, return false. Otherwise, return true.

    if(counter == 0)
    {
        return true
    }

    else
    {
        return false
    }
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    if(age < 13)
    {
        return undefined
    }

    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    else if(age >= 13 && age <=21)
    {
        let discountedPrice = 0.20*price.toFixed(2)
        let finalPrice = price.toFixed(2) - discountedPrice.toFixed(2)
        return finalPrice.toFixed(2)
    }

    else if(age >21 && age <65)
    {
        return price.toFixed(2)
    }
    
    else
    {
        let discountedPrice = 0.20*price.toFixed(2)
        let finalPrice = price.toFixed(2) - discountedPrice.toFixed(2)
        return finalPrice.toFixed(2)
    }
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string. 
    //(No need to parseInt???)
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.

    const hotCategory = items.filter(element => element.stocks == 0 )

    let arrayFiltered =[]
    let final =[]
    
    for(let i=0; i<hotCategory.length; i++){

        arrayFiltered.push(hotCategory[i].category)
    
    }

    // The hot categories must be unique; no repeating categories.

   /* for(let i=0; i<arrayFiltered.length; i++)
    {
        for(let j=0; j<arrayFiltered.length; j++){
            if(arrayFiltered[i] == hotCategory[i].length)
        }
       
    }

    console.log(final)
    return final*/
   
   let unique =[]
    arrayFiltered.forEach(element => 
    {
        if(!unique.includes(element)) 
        {
            unique.push(element)
        }
    })

    
    return unique
    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    let votedTwice = []

    for (let i=0; i<candidateA.length; i++)
    {
        for (let j=0; j<candidateB.length; j++)
        {
            if(candidateA[i] == candidateB[j])
            {
                votedTwice.push(candidateA[i])
            }
        }
    }

    return votedTwice
    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};